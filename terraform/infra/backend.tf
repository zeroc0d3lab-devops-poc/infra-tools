# Configure the Google Cloud tfstate file location
terraform {
  backend "gcs" {
    bucket = "terraform-golang-bookstore"
    prefix = "terraform"
  }
}
