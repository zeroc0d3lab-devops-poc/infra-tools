#!/bin/sh

### Create the remote back-end bucket in Cloud Storage for storage of the Terraform.tfstate file
gsutil mb -p ${TF_ADMIN} -l asia-southeast1 gs://${TF_ADMIN}

### Enable versioning for remote bucket:
gsutil versioning set on gs://${TF_ADMIN}

### Configure your environment for the Google Cloud Terraform provider
export GOOGLE_APPLICATION_CREDENTIALS=${TF_CREDS}
